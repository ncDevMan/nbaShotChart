
# This is a Data Visualization Project that will plot a certain players NBA Shot Chart.
# We will use Pandas, DataFrames, Seaborn, and Requests Python Libraries.
# For the demo we will use Anthony Davis as the example.



import pandas as pd;
import matplotlib as plt;
import seaborn as sns;
import requests;
from IPython.display import display;
from pylab import *;
from matplotlib.patches import Circle, Rectangle, Arc;
import urllib;
from matplotlib.offsetbox import OffsetImage;



# drawCourt() Function
# This function will Draw and NBA sized court using matplotlib
# @paramters: ax = Null (axis), color of lines, linewidth, if outerlines are wanted.

def drawCourt(ax = None, color = 'black', lw = 2, outerLines = False, playerID = ''):

    # If an axes object isnt provided to plot onto, just get current one.
    if ax is None:
        ax = plt.gca();

    # Let's create a NBA Basketball Court, shall we?


    #Basketball Hoop
    # Diameter of hoop is 18" so it has a radius of 9" which is = 7.5 here
    basketballHoop = Circle((0, 0), radius=7.5, linewidth=lw, color=color, fill=False);

    # Create backboard of the Goal
    backBoard = Rectangle((-30, -7.5), 60, -1, linewidth=lw, color=color);

    # THE PAINT
    # The outer box first!!
    #Outer Box - width = 16ft, height = 19ft
    outerPaintBox = Rectangle((-80, -47.5), 160, 190, linewidth=lw, color=color,fill=False);

    # The Inner box second!!
    # Inner Box - width = 12ft, height = 19ft
    innerPaintBox = Rectangle((-60, -47.5), 120, 190, linewidth=lw, color=color, fill=False);

    # Create free throw top arc
    topFreethrowArc = Arc((0, 142.5), 120, 120, theta1=0, theta2=180, linewidth=lw, color=color, fill=False);

    #Create freethrow bottom arc
    bottomFreethrowArc = Arc((0, 142.5), 120, 120, theta1=180, theta2=0, linewidth=lw, color=color, linestyle='dashed');

    # Restricted Zone, Arc with 4ft radius from center of the hoop.
    restrictedArea = Arc((0, 0), 80, 80, theta1=0, theta2=180, linewidth=lw, color=color);


    # Three point line
    # Create side 3point lines, they are 14 ft long before the begin to Arc
    cornerTreeA = Rectangle((-220, -47.5), 0, 140, linewidth=lw, color=color);
    cornerTreeB = Rectangle((220, -47.5), 0, 140, linewidth=lw, color=color);

    # Three Arc
    threeArc = Arc((0, 0), 475, 475, theta1=22, theta2=158, linewidth=lw, color=color);

    #Center Court
    centerOuterArc = Arc((0, 395), 120, 120, theta1=180, theta2=0, linewidth=lw, color=color);
    centerInnerArc = Arc((0, 395), 40, 40, theta1=180, theta2=0,linewidth=lw, color=color);

    courtElements = [basketballHoop, backBoard, outerPaintBox, innerPaintBox, topFreethrowArc, bottomFreethrowArc,
                     restrictedArea, cornerTreeA, cornerTreeB, threeArc, centerOuterArc, centerInnerArc]
    if outerLines:

        # Draw the half court line, baseline and side out bound lines.
        outerLines = Rectangle((-250, -47.5), 500, 442.5, linewidth = lw, color = color, fill = False);

        #Then append the lines to the court.
        courtElements.append(outerLines);

    for element in courtElements:
        ax.add_patch(element);

    return ax;




# ----------------------------------Main ----------------------------------

print ("Welcome to NBA Shot Data!");


# We can change player's IDs this way!
# This is grabbed from http://stats.nba.com/players/.

# 203076 = Anthony Davis
playerID = str(203076);

# This is using the requests python library.
# We are grabbing a JSON file.
shotChartURL = 'http://stats.nba.com/stats/shotchartdetail?CFID=33&CFPAR'\
                'AMS=2014-15&ContextFilter=&ContextMeasure=FGA&DateFrom=&D'\
                'ateTo=&GameID=&GameSegment=&LastNGames=0&LeagueID=00&Loca'\
                'tion=&MeasureType=Base&Month=0&OpponentTeamID=0&Outcome=&'\
                'PaceAdjust=N&PerMode=PerGame&Period=0&PlayerID='+str(playerID)+'&Plu'\
                'sMinus=N&Position=&Rank=N&RookieYear=&Season=2014-15&Seas'\
                'onSegment=&SeasonType=Regular+Season&TeamID=0&VsConferenc'\
                'e=&VsDivision=&mode=Advanced&showDetails=0&showShots=1&sh'\
                'owZones=0';


#Grab the webpage containing the data.
# This file will be a JSON file.
response = requests.get(shotChartURL);

#Grab the headers from the JSON to be used as column headers for our DataFrame
headers = response.json()['resultSets'][0]['headers'];

#Grab the shot chart data from the JSON file
shots = response.json()['resultSets'][0]['rowSet'];


shotDataFrame = pd.DataFrame(shots, columns = headers)

#View the head of the DataFrame and all its columns.
cmap=plt.cm.YlOrRd_r

with pd.option_context('display.max_columns', None):

    joint_shot_chart = sns.jointplot(shotDataFrame.LOC_X, shotDataFrame.LOC_Y, stat_func=None,
                                 kind='kde', space=0, color=cmap(0.1),
                                 cmap=cmap, n_levels=50)

    joint_shot_chart.fig.set_size_inches(12,11)
    # A joint plot has 3 Axes, the first one called ax_joint
    # is the one we want to draw our court onto and adjust some other settings
    ax = joint_shot_chart.ax_joint
    drawCourt(ax)

    # Adjust the axis limits and orientation of the plot in order
    # to plot half court, with the hoop by the top of the plot
    ax.set_xlim(-250,250)
    ax.set_ylim(395, -47.5)

    # Get rid of axis labels and tick marks
    ax.set_xlabel('')
    ax.set_ylabel('')
    ax.tick_params(labelbottom='off', labelleft='off')

    # Add a title
    ax.set_title('James Harden FGA \n2014-15 Reg. Season',
                 y=1.2, fontsize=18)

    # ax.text(-250, 435, 'Data Source: stats.nba.com' '\nAuthor: Nick Conn', fontsize = 12);

    playerPicture = urllib.urlretrieve("http://stats.nba.com/media/players/230x185/"+str(playerID)+".png",
                                str(playerID)+".png");

    playerPictureScrape = plt.imread(playerPicture[0]);



    # Plot shots from only right side of court.

    # right = shotDataFrame[shotDataFrame.SHOT_ZONE_AREA == 'Right Side(R)']
    # plt.figure(figsize=(12,11));
    # plt.scatter(right.LOC_X, right.LOC_Y);
    # plt.xlim(-300,300);
    # plt.ylim(-100, 500);
    # plt.show();

    # Plot shots from everywhere
    # With Outer Lines

    # display(shotDataFrame.head());
    # sns.set_style("white");
    # sns.set_color_codes();
    # plt.figure(figsize=(12,11));
    # drawCourt(outerLines = True);
    # plt.scatter(shotDataFrame.LOC_X, shotDataFrame.LOC_Y);
    # plt.xlim(300, -300);
    # plt.show();




    # Plot shots from only left side of court.

    # left = shotDataFrame[shotDataFrame.SHOT_ZONE_AREA == 'Left Side(L)']
    # plt.figure(figsize=(12,11));
    # plt.scatter(left.LOC_X, left.LOC_Y);
    # plt.xlim(-300,300);
    # plt.ylim(-100, 500);
    # plt.show();


    # Plot shots from everywhere
    # Up to HALF Court
    display(shotDataFrame.head());
    sns.set_style("white");
    sns.set_color_codes();
    plt.figure(figsize=(12,11));

    #No argument passed
    drawCourt();

    plt.scatter(shotDataFrame.LOC_X, shotDataFrame.LOC_Y);
    # ----------------------
    plt.xlim(-250, 250);
    plt.ylim(395, -47.5);
    # ----------------------
    plt.show();







print ("Thanks for checking out the NBA Data Machine!");